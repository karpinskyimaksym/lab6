import "./App.css";
import Login from "../Login/Login";
import {Route, Routes} from "react-router-dom";
import Dashboard from "../Dashboard/Dashboard";

function App() {
  return (
    <div className="App">
        <Routes>
            <Route path="dashboard" element={<Dashboard/>} />
            <Route path="/" element={<Login/>} />
        </Routes>
    </div>
  );
}

export default App;
