import "./Login.css";
import {useNavigate} from 'react-router-dom';
import Cookies from "js-cookie";

function Login(){

    const navigate = useNavigate();
    Cookies.remove("logged");

    const check = (login, password)=>{
        const log = "max3";
        const pass = "max3";

        if (login === log && password === pass) {
            Cookies.set("logged", "true");
            navigate('/dashboard');
        }
        else document.getElementById('error').innerText = 'Неправильний логін або пароль!';
    }

    return(
        <div className="form-container">
            <form className="login-form" onSubmit={(e) => {
                e.preventDefault();
                let l = document.getElementById('login').value;
                let p = document.getElementById('password').value;
                check(l, p);
            }}>
                <h2 className="log-to-web">Log to Web App</h2>
                <hr className="form-hr" />
                <div className="form-div">
                    <label htmlFor="login">E-mail:</label>
                    <input placeholder="name@example.com" type="text" id="login"/>
                </div>
                <hr className="form-hr" />
                <div className="form-div">
                    <label htmlFor="password">Password:</label>
                    <input placeholder="password" type="password" id="password"/>
                </div>
                <hr className="form-hr" />
                <input type='checkbox' id="remember"/>
                <label htmlFor="remember">Remember me</label>
                <p id="error"></p>
                <div className="form-div">
                    <button className="form-button">Login</button>
                </div>
            </form>
        </div>
    )
}

export default Login;
