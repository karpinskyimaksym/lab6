import "./WeatherElement.css";

function WeatherElement(props) {
    const item = props.day;
    const today = new Date().getDay();
    const itemDate= new Date(item.date).getDay();

    let date;
    if(today === itemDate) date="Today";
    else if(today+1 === itemDate) date="Tomorrow";
    else date = new Date(item.date).toLocaleDateString("en-US", { weekday: "short",  day: "numeric", month: "short" });

    return (
        <div className="weather-element">
            <h3 className="date">{date}</h3>
            <div className="icon">
                <img src={item.day.condition.icon} alt="weather picture"/>
                <p>{item.day.condition.text}</p>
            </div>
            <div>Max: {item.day.maxtemp_c}°C<br/>Min: {item.day.mintemp_c}°C</div>
            <div><h4>Wind speed</h4>{item.day.maxwind_mph} mph</div>
            <div><h4>Humidity</h4>{item.day.avghumidity}%</div>
            <div><h4>Visibility</h4>{item.day.avgvis_miles} miles</div>
            <div><h4>Pressure</h4>{item.hour[0].pressure_mb} mb</div>
            <div><h4>Confidence</h4>{item.day.daily_chance_of_rain}%</div>
        </div>
    );
}

export default WeatherElement;
