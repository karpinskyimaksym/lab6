import "./Dashboard.css";
import {Bars} from 'react-loader-spinner'
import {useEffect, useState} from 'react';
import Cookies from "js-cookie";
import {useNavigate} from "react-router-dom";
import WeatherElement from "../WeatherElement/WeatherElement";

function Dashboard() {

    const [weather, setWeather] = useState(null);
    const navigate = useNavigate();

    useEffect(() => {
        if (!Cookies.get("logged")) {
            alert("Необхідна авторизація!");
            navigate("/");
        }

        navigator.geolocation.getCurrentPosition(
            (position) => {
            fetch(`https://api.weatherapi.com/v1/forecast.json?key=f99baae0ef1a4d1187a94526231511&q=
            ${position.coords.latitude},${position.coords.longitude}&days=5&aqi=no&alerts=yess`)
                .then(data => data.json())
                .then(data => setWeather(data))
            },
            (ignored) => {
                alert("Надайте дозвіл для отримання даних геолокації!");
            }
        );
    }, [navigate]);

    if(weather)
    return (
        <div className="Dashboard">
            <header>
                <img src="//cdn.weatherapi.com/weather/64x64/day/116.png" alt="logo" />
                <h4>MetaWeather</h4>
                <div className="menu">
                    <h4>Home</h4>
                    <h4>Language</h4>
                    <h4>Map</h4>
                    <h4>API</h4>
                    <h4>About</h4>
                </div>
                <input type="text" className="search" placeholder="Search" />
            </header>
            <main>
                <div className="top">
                    <div className="left-info"><h1 className="city">{weather.location.name}</h1>, {weather.location.country}</div>
                    <div className="right-info">
                        <div><h4 className="category">Time</h4> {new Date(weather.current.last_updated).toLocaleTimeString()}</div>
                        <div><h4 className="category">Sunrise</h4>  {weather.forecast.forecastday[0].astro.sunrise}</div>
                        <div><h4 className="category">Sunset</h4> {weather.forecast.forecastday[0].astro.sunset}</div>
                    </div>
                </div>
                <div className="weather">
                    {weather.forecast.forecastday.map(day => <WeatherElement day={day} key={day.date} />)}
                </div>
            </main>
        </div>
    );

    return (
        <div className="loader">
            <Bars height="160" width="160" color="blue" />
            <p>Зачекайте, будь ласка, триває завантаження</p>
        </div>
    );
}

export default Dashboard;
